<?php

namespace Drupal\amazon_ses_list;

use Drupal\node\NodeInterface;

/**
 * Interface for mailout service.
 */
interface MailoutInterface {

  /**
   * AWS has a rate limit on many functions so this is a standard
   * wait time in milliseconds to be used when API calls need to be made
   * in rapid succession.
   * Current: 1 seconds.
   */
  const RATE_LIMIT = 1000000;

  /**
   * Assigns the mailout node being processed.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node being processed for an Amazon SES mailout.
   */
  public function setNode(NodeInterface $node);

  /**
   * Processes a given node for Amazon SES.
   */
  public function sendMailout();

  /**
   * Send test email for a given mailout node.
   */
  public function sendTest();

}
