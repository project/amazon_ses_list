<?php

namespace Drupal\amazon_ses_list\Form;

use Aws\SesV2\Exception\SesV2Exception;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\amazon_ses_list\MailoutInterface;

/**
 * LINZ SES verified identities form.
 */
class SesListImportSubscribersForm extends SesListFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_ses_list_import_subscribers_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upload'] = [
      '#type' => 'file',
      '#title' => $this->t('File'),
      '#description' => $this->t('The XML file with headers. It must contain an "email" header and all other columns will be stored as a JSON string in subscription attributes data.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Import'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $files = $this->getRequest()->files->get('files', []);
    $file = $files['upload'];

    $xml = simplexml_load_file($file->getPathname());

    $batch = [
      'title' => t('Importing subscribers'),
      'operations' => [],
      'finished' => ['\Drupal\amazon_ses_list\Form\SesListImportSubscribersForm', 'importComplete'],
      'init_message' => t('Import process is starting.'),
      'progress_message' => t('Processed @current out of @total. Estimated time: @estimate.'),
      'error_message' => t('The process has encountered an error.'),
    ];

    foreach ($xml->node as $sub) {
      // There must be an "email" key in the imported file.
      $contact = [
        'AttributesData' => json_encode($sub),
        'ContactListName' => \Drupal::config('amazon_ses_list.settings')->get('list_name'),
        'EmailAddress' => (string) $sub->email,
      ];

      $batch['operations'][] = [
        ['\Drupal\amazon_ses_list\Form\SesListImportSubscribersForm',
        'importSubscriber'],
        [$contact],
      ];
    }

    batch_set($batch);
  }

  /**
   * @todo
   */
  protected function convertChartIds($legacyIds) {
    $query = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'chart')
      ->condition('field_legacy_nid', $legacyIds, 'IN');
    $results = $query->execute();

    return $results;
  }

  /**
   * Static function called by batch that makes the Amazon API calls.
   */
  public static function importSubscriber($contact) {
    $client = \Drupal::service('amazon_ses.client');

    try {
      $client->createContact($contact);
    }
    catch (SesV2Exception $e) {
      // The rate limit was hit and we need to wait and retry.
      if ($e->getCode() === 'TooManyRequestsException') {
        usleep(MailoutInterface::RATE_LIMIT);
        try {
          $client->createContact($contact);
        }
        catch (SesV2Exception $e) {
          \Drupal::messenger()->addError($e->getAwsErrorMessage());
          \Drupal::logger('amazon_ses_list')->error($e->getAwsErrorMessage());
        }
      }
      // The contact exists so try to update it.
      elseif ($e->getCode() === 'AlreadyExistsException') {
        $firstError = $e->getAwsErrorMessage();
        try {
          $client->updateContact($contact);
        }
        catch (SesV2Exception $e) {
          \Drupal::messenger()->addError($e->getAwsErrorMessage());
          \Drupal::logger('amazon_ses_list')->error($e->getAwsErrorMessage());
        }
      }
    }

    // Backoff before proceeding to avoid API rate limits.
    usleep(MailoutInterface::RATE_LIMIT);
  }

  /**
   * @todo
   */
  public static function importComplete($success, $results, $operations) {
    \Drupal::messenger()->addMessage(t('File uploaded and processed.'));
  }

}
