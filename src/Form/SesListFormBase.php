<?php

namespace Drupal\amazon_ses_list\Form;

use Drupal\Core\Form\FormBase;
use Drupal\amazon_ses\Traits\HandlerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Linz SES form base class.
 */
abstract class SesListFormBase extends FormBase {
  use HandlerTrait;

  /**
   * Client
   */
  protected $client;

  /**
   * Pager
   */
  protected $pager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->setHandler($container->get('amazon_ses.handler'));

    $instance->client = $container->get('amazon_ses.client');
    $instance->pager = $container->get('pager.manager');

    return $instance;
  }

}
