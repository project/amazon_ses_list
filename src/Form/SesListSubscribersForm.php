<?php

namespace Drupal\amazon_ses_list\Form;

use Aws\SesV2\Exception\SesV2Exception;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\amazon_ses_list\MailoutInterface;

/**
 * LINZ SES verified identities form.
 */
class SesListSubscribersForm extends SesListFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_ses_list_subscribers_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $searchEmail = \Drupal::request()->query->get('search');
    $form['filter'] = [
      '#type' => 'fieldset',
      'search_email' => [
        '#type' => 'textfield',
        '#default_value'=> $searchEmail,
        '#attributes' => [
          'placeholder' => 'Search by email',
        ],
      ],
      'search_submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
      ],
    ];

    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => ['delete' => $this->t('Delete')],
    ];

    $header = [
      'EmailAddress' => $this->t('Email'),
      'LastUpdatedTimestamp' => $this->t('Updated on'),
    ];

    $nextToken = NULL;
    $contacts = [];
    do {
      $contactResults = $this->client->listContacts([
        'ContactListName' => \Drupal::config('amazon_ses_list.settings')->get('list_name'),
        'NextToken' => $nextToken,
        'PageSize' => 1000,
      ]);

      foreach ($contactResults['Contacts'] as $lite) {
        $email = $lite['EmailAddress'];

        if ($searchEmail) {
          if (str_contains($email, $searchEmail)) {
            $contacts[$email] = $lite;
          }
          else {
            continue;
          }
        }
        else {
          $contacts[$email] = $lite;
        }
      }

      $nextToken = $contactResults['NextToken'];
      if ($nextToken) {
        // Backoff before proceeding to avoid API rate limits.
        usleep(MailoutInterface::RATE_LIMIT);
      }
    } while ($nextToken);

    // Sort contacts so newest shows first.
    usort($contacts, function($a, $b) {
      return new \DateTime($b['LastUpdatedTimestamp']) <=> new \DateTime($a['LastUpdatedTimestamp']);
    });

    $rows = $this->pagerArray($contacts, 50);

    $form['header'] = [
      '#markup' => $this->t('Showing @num of @total contacts.', [
        '@num' => count($rows),
        '@total' => count($contacts),
      ]),
    ];
    $form['subscribers'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('There are no verified subscribers.'),
    ];
    $form['pager'] = [
      '#type' => 'pager',
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Apply to selected subscribers'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $isAction = $formState->getTriggeringElement()['#id'] === 'edit-submit';
    $action = $formState->getValue('action');
    $search = $formState->getValue('search_email');

    if ($isAction && $action == 'delete') {
      $keys = array_filter($formState->getValue('subscribers'));
      foreach ($keys as $key) {
        $subscriber = $form['subscribers']['#options'][$key];
        try {
          $result = $this->client->deleteContact([
            'ContactListName' => $subscriber['ContactListName'],
            'EmailAddress' => $subscriber['EmailAddress'],
          ]);
        }
        catch (SesV2Exception $e) {
          $this->messenger()->addMessage($this->t($e->getAwsErrorMessage()));
        }
      }

      // Confirm only if $result was set at least once.
      if (isset($result)) {
        $this->messenger()
          ->addMessage($this->t('The identities have been deleted.'));
      }
    }

    if ($search) {
      $options = ['query' => ['search' => $search]];
    }
    $url = Url::fromRoute('amazon_ses_list.subscribers', [], $options ?? []);
    $formState->setRedirectUrl($url);
  }

  /**
   * Returns pager array.
   */
  public function pagerArray($items, $itemsPerPage) {
    if (empty($items)) {
      return [];
    }
    // Get total items count.
    $total = count($items);
    // Get the number of the current page.
    $currentPage = $this->pager->createPager($total, $itemsPerPage)
      ->getCurrentPage();
    // Split an array into chunks.
    $chunks = array_chunk($items, $itemsPerPage);
    // Return current group item.
    $currentPageItems = $chunks[$currentPage];

    return $currentPageItems;
  }

}
