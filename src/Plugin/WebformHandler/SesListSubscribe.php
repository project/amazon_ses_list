<?php

namespace Drupal\amazon_ses_list\Plugin\WebformHandler;

use Aws\SesV2\Exception\SesV2Exception;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Webform submission SES subscription manager.
 *
 * @WebformHandler(
 *   id = "amazon_ses_list_subscribe",
 *   label = @Translation("Amazon SES Subscribe"),
 *   category = @Translation("External"),
 *   description = @Translation("Subscribes submitter to Amazon SES."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class SesListSubscribe extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $submission, $update = TRUE) {
    $email = $submission->getElementData('email');
    $listId = $this->configuration['list'];
    $client = \Drupal::service('amazon_ses.client');
    $data = $submission->getData();
    $data['uuid'] = $submission->uuid();
    // We don't need the email key taking up space in $data.
    unset($data['email']);

    // @todo Use excluded_data to limit what attributes are stored.
    // @todo $data is limitted to 1000 characters by AWS so we should
    // show a friendly error to the user when that limit is passed.
    $contact = [
      'AttributesData' => json_encode($data, JSON_NUMERIC_CHECK),
      'ContactListName' => $listId,
      'EmailAddress' => $email,
    ];

    try {
      $result = $client->createContact($contact);
    }
    catch (SesV2Exception $e) {
      \Drupal::messenger()->addMessage($e->getAwsErrorMessage(), 'error');
      \Drupal::logger('amazon_ses_list')->error($e->getAwsErrorMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $settings = $this->getSettings();

    return [
      '#markup' => 'List: ' . $settings['list'] ?? 'undefined',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // @todo Determine why entity field manager dependency can't be injected.
    $fieldNames = array_keys(\Drupal::service('entity_field.manager')->getBaseFieldDefinitions('webform_submission'));
    $excludedData = array_combine($fieldNames, $fieldNames);

    return [
      'list' => '',
      'results_disabled' => $this->isResultsEnabled(),
      'excluded_data' => $excludedData,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $client = \Drupal::service('amazon_ses.client');
    $options = [];

    try {
      $lists = $client->listContactLists();
      foreach ($lists['ContactLists'] as $list) {
        $name = $list['ContactListName'];
        $options[$name] = $name;
      }
      // @todo for ease of getting things done, we hardcode list creation here
      // if it doesn't exist for some reason.

    }
    catch (SesV2Exception $e) {
      \Drupal::messenger()->addMessage($e->getAwsErrorMessage(), 'error');
    }

    $form['ses'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Amazon SES Settings'),
    ];

    // @todo make this a dropdown based on list IDs.
    $form['ses']['list'] = [
      '#type' => 'select',
      '#title' => $this->t('List'),
      '#description' => $this->t('The Amazon SES list ID to subscribe to. Lists can only be added, updated, and deleted via the Amazon CLI.'),
      '#options' => $options,
      '#required' => TRUE,
      '#maxlength' => NULL,
      '#default_value' => $this->configuration['list'],
    ];

    $form['save'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Submission Saving'),
    ];
    $form['save']['results_disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable saving of submissions'),
      '#description' => $this->t('Change the setting on the <em>General</em> tab of the webform settings. It is highly recommended to disable saving of submissions.'),
      '#disabled' => TRUE,
      '#default_value' => !$this->isResultsEnabled(),
    ];

    // Submission data.
    $form['submission_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Submission data'),
    ];
    $form['submission_data']['excluded_data'] = [
      '#type' => 'webform_excluded_columns',
      '#title' => $this->t('Posted data'),
      '#title_display' => 'invisible',
      '#webform_id' => $this->getWebform()->id(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['excluded_data'],
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $formState) {
    parent::submitConfigurationForm($form, $formState);
    $this->applyFormStateToConfiguration($formState);
  }

  /**
   * Determine if saving of results is enabled.
   *
   * @return bool
   *   TRUE if saving of results is enabled.
   */
  protected function isResultsEnabled() {
    return $this->getWebform() ? ($this->getWebform()->getSetting('results_disabled') === FALSE) : NULL;
  }

}
