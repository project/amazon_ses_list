<?php

namespace Drupal\amazon_ses_list\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * LINZ SES mail queue worker.
 *
 * @QueueWorker(
 *   id = "amazon_ses_list_mail_queue",
 *   title = @Translation("NtM Edition mail queue"),
 *   cron = {"time" = 60}
 * )
 */
class NtmMailQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The mailout node to mailout.
   *
   * @var \Drupal\amazon_ses_list\MailoutInterface $mailout
   */
  protected $mailout;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->mailout = $container->get('amazon_ses_list.mailout');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    $node = $this->mailout->loadLatestRevision($item['nid']);
    $this->mailout->setNode($node);
    $this->mailout->sendEmail($item['recipient'], $item['data']);

    // Surely we can load this from $this?
    $remaining = \Drupal::service('queue')->get('amazon_ses_list_mail_queue')
      ->numberOfItems();
    if ($remaining === 1) {
      $timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
      $now = new \DateTime('now', $timezone);
      $date = $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      // @todo change status to published/finished
      $node->set('field_emails_sent_date', $date);
      $node->save();
    }
  }

}
