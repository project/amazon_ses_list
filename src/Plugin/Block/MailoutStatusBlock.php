<?php

namespace Drupal\amazon_ses_list\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block that gives context on Mailout send status.
 *
 * @Block(
 *   id = "amazon_ses_list_mailout_status",
 *   admin_label = @Translation("NtM Edition mailout status"),
 *   category = @Translation("LINZ Blocks")
 * )
 */
class MailoutStatusBlock extends BlockBase {

 /**
   * {@inheritdoc}
   */
  public function build() {
    // Get node from route.
    $node = \Drupal::routeMatch()->getParameter('node');
    if (!$node || $node->bundle() !== 'mailout') {
      return [];
    }

    $isPublished = $node->isPublished();
    // Get the latest revision from storage as we only care about
    // the last set values on this node
    // @todo publish the node when we set these values elsewhere
    // so we only need to care about what's published.
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $revisionId = $nodeStorage->getLatestRevisionId($node->id());
    $node = $nodeStorage->loadRevision($revisionId);

    $status = '';
    $queued = $node->field_emails_queued->value ?? 0;
    $finishedDate = $node->field_emails_sent_date->date ?? FALSE;

    if (!$queued && $isPublished) {
      $status = 'Edition has been published but not yet sent.';
    }
    elseif ($finishedDate) {
      $timezone = new \DateTimeZone(date_default_timezone_get());
      $finishedDate->setTimezone($timezone);
      $status = $this->t('Sent @x emails on @date.', [
        '@x' => $queued,
        '@date' => $finishedDate->format('l j  F \a\t g:ia'),
      ]);
    }
    elseif ($queued > 0) {
      // We don't know for sure the items in the queue are for this node,
      // but since $finishedDate hasn't been set on this node we can assume.
      $leftToSend = \Drupal::service('queue')->get('amazon_ses_list_mail_queue')
        ->numberOfItems();
      $totalSent = max($queued - $leftToSend, 0);
      $status = $this->t('Queued @x emails. Sent @sent with @left remaining.', [
        '@x' => $queued,
        '@sent' => $totalSent,
        '@left' => $leftToSend,
      ]);
    }

    $build = [
      '#type' => 'div',
      '#markup' => $status,
    ];

    return $status ? $build : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // @todo we do want to cache this under certain conditions though
    return 0;
  }

}
