<?php

namespace Drupal\amazon_ses_list;

use AWS\SesV2\SesV2Client;
use AWS\SesV2\Exception\SesV2Exception;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\Component\Render\PlainTextOutput;
use Symfony\Component\Finder\SplFileInfo;

/**
 * The service to process NtM mailouts.
 */
class Mailout implements MailoutInterface {

  use StringTranslationTrait;
  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * The AWS client to use.
   *
   * @var \AWS\SesV2\SesV2Client $client
   */
  protected $client;

  /**
   * The mailout node to mailout.
   *
   * @var \Drupal\node\NodeInterface $node
   */
  protected $node;

  /**
   * Creates a new Mailout instance.
   *
   * @param \AWS\SesV2\SesV2Client $client
   *   The AWS SesV2 client.
   */
  public function __construct(SesV2Client $client) {
    $this->client = $client;
    $this->config = \Drupal::config('amazon_ses_list.settings');
  }

  /**
   * @todo
   */
  public function setNode(NodeInterface $node) {
    $this->node = $node;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareMailout() {
      // Allows data replacements to be added in override to pass to template.
      return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sendTest() {
    $email =  $this->node->get('field_test_recipient')->value;
    if (!$email) {
      $this->messenger()->addError('Unable to send test email as no recipient has been set.');
      return;
    }
    $data = $this->prepareMailout();

    $this->sendEmail($email, $data, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function sendMailout() {
    // @todo this function has become 'queueMailout'.
    $this->getLogger('mailout update')
      ->notice('Started queueing emails for ' . $this->node->getTitle());

    $queue = \Drupal::service('queue')->get('amazon_ses_list_mail_queue');
    $data = $this->prepareMailout();
    $queued = 0;
    $nextToken = NULL;

    // @todo Have a way to interrupt the queue worked if node is set to
    // cancelled state, OR use the Queue UI for that?
    // @todo Investigate async sending as that might make this very fast?
    do {
      $contactResults = $this->client->listContacts([
        'ContactListName' => $this->config->get('list_name'),
        'Filter' => [
          'FilteredStatus' => 'OPT_IN',
        ],
        'NextToken' => $nextToken,
        'PageSize' => 1000,
      ]);

      foreach ($contactResults['Contacts'] as $contact) {
        $queue->createItem([
          'recipient' => $contact['EmailAddress'],
          'data' => $data,
          'nid' => $this->node->id(),
        ]);
        $queued++;
      }
      $nextToken = $contactResults['NextToken'];
      if ($nextToken) {
        // Backoff before proceeding to avoid API rate limits.
        usleep(MailoutInterface::RATE_LIMIT);
      }
    } while ($nextToken);

    $this->getLogger('mailout update')
      ->notice('Finished queueing @count emails for @node', [
        '@count' => $queued,
        '@node' => $this->node->getTitle(),
      ]);
    if ($this->node->hasField('field_emails_queued')) {
      $this->node->set('field_emails_queued', $queued);
    }
  }

  /**
   * @todo
   */
  protected function getContactAttributes($mail) {
    // Backoff before proceeding to avoid API rate limits.
    // Unfortunately we can't bulk load contact data.
    usleep(MailoutInterface::RATE_LIMIT);

    $data = [];
    try {
      $contact = $this->client->getContact([
        'ContactListName' => $this->config->get('list_name'),
        'EmailAddress' => $mail,
      ]);

      $data = $contact['AttributesData'] ? json_decode($contact['AttributesData'], TRUE) : [];
    }
    catch (SesV2Exception $e) {
      $this->getLogger('amazon_ses_list')->error($e->getAwsErrorMessage());
    }

    return $data;
  }

  /**
   * Handles the sending of a processed notice email.
   */
  public function sendEmail(string $recipient, array $templateData, bool $withAttributes = TRUE) {
    if ($withAttributes) {
      // Override the data array and append it after so that contact data
      // always takes precidence.
      $templateData = $this->getContactAttributes($recipient) + $templateData;
    }

    $sesConfig = \Drupal::config('amazon_ses.settings');
    $email = [
      //'ConfigurationSetName' => '<string>',
      'Content' => [
        'Template' => [
          'TemplateData' => json_encode($templateData),
          'TemplateName' => $this->config->get('template_name'),
        ],
      ],
      'Destination' => [
        'ToAddresses' => [$recipient],
      ],
      'FromEmailAddress' => $sesConfig->get('from_address'),
      'ListManagementOptions' => [
        'ContactListName' => $this->config->get('list_name'),
      ],
    ];

    // @todo Check throttle setting before sending.
    $maxSendRate = $this->client->getAccount()['SendQuota']['MaxSendRate'];
    usleep(intval(ceil(1000000 / $maxSendRate)));

    try {
      $result = $this->client->sendEmail($email);
    }
    catch (SesV2Exception $e) {
      $this->getLogger('amazon_ses_list')->error($e->getAwsErrorMessage());
    }
  }

  /**
   * Syncs raw HTML file that AWS will use {{var}} token replacements on.
   */
  protected function updateTemplate() {
    // Drupal does not replace the template so no hook_theme.
    $modulePath = \Drupal::service('module_handler')
      ->getModule('amazon_ses_list')
      ->getPath();
    $templatePath = '/templates/ses-email-template.html';
    $templateFile = new SplFileInfo($modulePath . $templatePath, '', '');

    $htmlBody = $templateFile->getContents();
    $subject = $this->node->getTitle();
    $plainBody = PlainTextOutput::renderFromHtml($htmlBody);
    // @todo could use this to strip empty newlines.
    //$str = implode("\n", array_filter(explode("\n", $str)));

    try {
      $result = $this->client->updateEmailTemplate([
        'TemplateName' => 'NZ_Notices_to_Mariners',
        'TemplateContent' => [
          'Html' => $htmlBody,
          'Subject' => $subject,
          'Text' => $plainBody,
        ],
      ]);
    }
    catch (SesV2Exception $e) {
      $this->getLogger('amazon_ses_list')->error($e->getAwsErrorMessage());
      // Assume failure is because template doesn't exist yet.
      $result = $this->client->createEmailTemplate([
        'TemplateName' => 'NZ_Notices_to_Mariners',
        'TemplateContent' => [
          'Html' => $htmlBody,
          'Subject' => $subject,
          'Text' => $plainBody,
        ],
      ]);
    }

    return $result;
  }


}
